# -*- coding: utf-8 -*-
"""
Creates the video player dialog
"""
import sqlite3
from pathlib import Path

from PyQt5.QtGui import QIcon, QFont
from PyQt5.QtCore import QDir, Qt, QUrl, QSize
from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtWidgets import (QApplication, QFileDialog, QHBoxLayout, QLabel,
        QPushButton, QSizePolicy, QSlider, QStyle, QVBoxLayout, QWidget, QStatusBar, QDesktopWidget, QMessageBox)


class VideoPlayer(QWidget):

    def __init__(self, parent=None):
        super(VideoPlayer, self).__init__(parent)

        self.mediaPlayer = QMediaPlayer(None, QMediaPlayer.VideoSurface)
        self.mediaPlayer.error.connect(self.handleError)

        btnSize = QSize(16, 16)
        self.playButton = QPushButton()
        self.playButton.setEnabled(False)
        self.playButton.setFixedHeight(24)
        self.playButton.setIconSize(btnSize)
        self.playButton.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
        self.playButton.clicked.connect(self.play)

        self.positionSlider = QSlider(Qt.Horizontal)
        self.positionSlider.setRange(0, 0)
        self.positionSlider.sliderMoved.connect(self.setPosition)

        self.statusBar = QStatusBar()
        self.statusBar.setFont(QFont("Noto Sans", 7))
        self.statusBar.setFixedHeight(14)

        controlLayout = QHBoxLayout()
        controlLayout.setContentsMargins(0, 0, 0, 0)
        controlLayout.addWidget(self.playButton)
        controlLayout.addWidget(self.positionSlider)

        layout = QVBoxLayout()
        videoWidget = QVideoWidget()
        layout.addWidget(videoWidget)
        layout.addLayout(controlLayout)
        layout.addWidget(self.statusBar)
        self.setLayout(layout)

        self.mediaPlayer.setVideoOutput(videoWidget)
        self.mediaPlayer.stateChanged.connect(self.mediaStateChanged)
        self.mediaPlayer.positionChanged.connect(self.positionChanged)
        self.mediaPlayer.durationChanged.connect(self.durationChanged)
        self.mediaPlayer.mediaStatusChanged.connect(self.mediaStatusChanged)

        # Resize to 75% of screen on startup
        fraction = .75
        widget = QDesktopWidget();
        screenGeometry = widget.screenGeometry();
        width = int(fraction * screenGeometry.width())
        height = int(fraction * screenGeometry.height())
        self.resize(width, height)
        self.move(int(width / 10), int(height / 10))

    def setVideo(self, filename, frame):
        """ Loads a new video and sets it up
        """
        self.setWindowTitle(Path(filename).name)
        self.statusBar.showMessage("Loading")
        content = QMediaContent(QUrl.fromLocalFile(filename))
        if (content.isNull()):
            print("Content is null")
        self.filename = filename
        self.frame = frame
        self.mediaPlayer.setMedia(content)
        # Remaining setup occurs when media has finished loading. This is triggered by the media status changed event

    def mediaStatusChanged(self, status):

        print("Media status changed: " + str(status))
        # Handle media loaded event
        if status == QMediaPlayer.MediaStatus.InvalidMedia:
            msg = QMessageBox()
            msg.setWindowTitle("Groundskeeper Bridge")
            txt = "Couldn't load file, " + self.filename + ".\n\n" \
                  "Under Windows, this often means necessary codecs aren't installed.\n\n" \
                  "Please try installing K-Lite Codec Pack (Standard):\n" \
                  "https://files3.codecguide.com/K-Lite_Codec_Pack_1670_Standard.exe \n\n" \
                  "Then, restart QGIS application and try again."
            msg.setText(txt)
            x = msg.exec_()
            return

        if status != QMediaPlayer.MediaStatus.LoadedMedia:
            return

        self.positionSlider.setRange(0, self.mediaPlayer.duration())

        # Apparently there is no method in QMediaPlayer to either get the default playback rate or the total number of
        # frames in the video. As a work around, use the co-located frame database to determine the number of frames.
        # Then use this to calculate fps for position calculations
        db_file = Path(self.filename).with_suffix(".sqlite")
        position = 0
        if db_file.exists():
            db = sqlite3.connect(str(db_file))
            result = db.execute("SELECT MAX(frame) FROM frames")
            row = result.fetchone();
            if row:
                max_frame = row[0]
                position = int(float(self.frame) / float(max_frame) * float(self.mediaPlayer.duration()))
                print("Frame = %d  Max frame = %d  Position = %d  Duration: %d" %
                      (self.frame, max_frame, position, self.mediaPlayer.duration()))
                self.mediaPlayer.setPosition(position)
                self.positionSlider.setValue(position)
                self.statusBar.showMessage("Frame %i  Position = %i" % (self.frame, position))
            else:
                self.mediaPlayer.setPosition(0)
                self.statusBar.showMessage("Frame table for %s is empty. Can't find frame %d position. Position = 0" %
                                           (db_file, self.frame))
        else:
            self.mediaPlayer.setPosition(0)
            self.statusBar.showMessage("%s does not exist. Can't find frame %d position. Position = 0" %
                                       (db_file, self.frame))

        self.mediaPlayer.pause()
        self.playButton.setEnabled(True)

    def play(self):
        if self.mediaPlayer.state() == QMediaPlayer.PlayingState:
            self.mediaPlayer.pause()
        else:
            self.mediaPlayer.play()

    def mediaStateChanged(self, state):
        #print("Media player state changed: " + str(state))
        if self.mediaPlayer.state() == QMediaPlayer.PlayingState:
            self.playButton.setIcon(
                    self.style().standardIcon(QStyle.SP_MediaPause))
        else:
            self.playButton.setIcon(
                    self.style().standardIcon(QStyle.SP_MediaPlay))

    def positionChanged(self, position):
        self.positionSlider.setValue(position)

    def durationChanged(self, duration):
        self.positionSlider.setRange(0, duration)

    def setPosition(self, position):
        self.mediaPlayer.setPosition(position)

    def handleError(self, error):
        print("MediaPlayerError: " + str(error))
        print("MediaPlayerError: " + self.mediaPlayer.errorString())
        if self.statusBar:
            self.statusBar.showMessage("Error: " + self.mediaPlayer.errorString())
        if self.playButton:
            self.playButton.setEnabled(False)

