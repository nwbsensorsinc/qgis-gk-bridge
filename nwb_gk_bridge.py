# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GroundskeeperBridge
                                 A QGIS plugin
 This plugin serves as a way to move data from the Groundskeeper systems into organized QGIS layers
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2021-08-09
        git sha              : $Format:%H$
        copyright            : (C) 2021 by Brendan Kristiansen, NWB Sensors, Inc
        email                : brendan.kristiansen@nwbsensors.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
import shutil
import sqlite3
import sys
from pathlib import Path
from typing import Dict, List, Union, Generator
import os
import os.path
import re
import csv
import json
import time
from math import cos, sin, radians, degrees, asin, atan2, pi

from qgis.PyQt.QtCore import QSettings, QTranslator, QCoreApplication
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction, QMessageBox, QProgressDialog, QProgressBar, QApplication
from qgis.gui import QgsMapToolEmitPoint, QgsFileWidget
from qgis.core import *

# Initialize Qt resources from file resources.py
from .resources import *
# Import the code for the dialog
from .nwb_gk_bridge_dialog import GroundskeeperBridgeDialog
from .nwb_video_player import VideoPlayer

class NWBPluginMode:
    import_mode = 0
    delete_mode = 1

def show_messagebox(text: str):
    msg = QMessageBox()
    msg.setWindowTitle("Groundskeeper Bridge")
    msg.setText(text)
    x = msg.exec_()


class GKDetection:
    _label: str
    _confidence: float
    _lat: float
    _lon: float

    def __init__(self, label: str, conf: float, lat: float, lon: float):
        """ Constructor

        :param label:
        :param conf:
        :param lat:
        :param lon:

        """

        self._label = str(label)
        self._confidence = conf
        self._lat = lat
        self._lon = lon

    @property
    def label(self):
        return self._label

    @property
    def confidence(self):
        return self._confidence

    @property
    def latitude(self):
        return self._lat

    @property
    def longitude(self):
        return self._lon


def calculateDerivedLocation(start_point: QgsPointXY, bearing: float, range: float) -> QgsPointXY:
    """
    startPoint:  Starting point where Y is latitude and X is longitude in degrees
    bearing:     Bearing in degrees from starting point
    range:       Range in meters from starting point
    @returns     Displaced point
    """
    EARTH_RADIUS = 6370986.884258304;
    ang_dist_rad = range / EARTH_RADIUS
    bearing_rad = radians(bearing)
    lat_rad = radians(start_point.y())
    lon_rad = radians(start_point.x())
    latB_rad = asin(sin(lat_rad) * cos(ang_dist_rad) + cos(lat_rad) * sin(ang_dist_rad) * cos(bearing_rad))
    dlon = atan2(sin(bearing_rad) * sin(ang_dist_rad) * cos(lat_rad), cos(ang_dist_rad) - sin(lat_rad) * sin(latB_rad))
    lonB_rad = ((lon_rad + dlon + pi) % (2*pi)) - pi
    return QgsPointXY(degrees(lonB_rad), degrees(latB_rad))

class ClassificationsSqliteConnection:
    _path: Path
    _cnx: sqlite3.Connection

    def __init__(self, location: Path):
        self._path = Path(location)
        self._cnx = sqlite3.connect(str(self._path))

    def get_labels(self) -> Dict[int, str]:
        res = self._cnx.execute("SELECT class, label FROM classifier_labels")
        labels = {}
        for row in res.fetchall():
            labels[row[0]] = row[1]
        return labels

    def get_all_classifications(self) -> Generator[GKDetection, None, None]:
        labels = self.get_labels()
        res = self._cnx.execute("SELECT best_class, best_confidence, latitude, longitude FROM classifications")
        for row in res.fetchall():
            label = str(row[0]) if labels == {} else labels[row[0]]
            yield GKDetection(label, row[1], row[2], row[3])

    def query(self, sql: str) -> List[tuple]:
        res = self._cnx.execute(sql)
        return res.fetchall()

class VideoFramesSqliteDatabase:
    _path: Path
    _cnx: sqlite3.Connection

    def __init__(self, location: Path):
        self._path = Path(location)
        self._cnx = sqlite3.connect(str(self._path))

        # Create table and index
        query = ("CREATE TABLE IF NOT EXISTS frames ("
                 "dt real, "
                 "latitude real, "
                 "longitude real, "
                 "filename text, "
                 "frame integer)")
        self._cnx.execute(query)
        query = "CREATE INDEX IF NOT EXISTS frames_index ON frames (latitude, longitude)"
        self._cnx.execute(query)

    def close(self):
        self._cnx.close();

    def merge_video_database(self, raw_db_file_path):
        """
        Merges a video database into the current video database
        """

        # Ensure the associated video file exists
        raw_video_file_path = raw_db_file_path.with_suffix(".mp4")
        if not raw_video_file_path.exists():
            print("Ignoring database with no associated video. File: %s", raw_db_file_path)
            return

        # Merge database
        self._cnx.execute("ATTACH DATABASE ? AS raw",(str(raw_db_file_path),));
        query = ("INSERT INTO main.frames (dt, latitude, longitude, filename, frame) "
                 "SELECT dt, lat as latitude, lon as longitude, ? AS filename, frame FROM raw.frames")
        self._cnx.execute(query, (str(raw_video_file_path),))
        self._cnx.commit()
        self._cnx.execute("DETACH DATABASE raw");

    def get_closest_frame(self, center_deg, max_dist_m) -> QgsPointXY:

        # Narrow down the set of results to those within a "square" around the center point
        p1 = calculateDerivedLocation(center_deg, 0, max_dist_m)
        p2 = calculateDerivedLocation(center_deg, 90, max_dist_m)
        p3 = calculateDerivedLocation(center_deg, 180, max_dist_m)
        p4 = calculateDerivedLocation(center_deg, 270, max_dist_m)
        query = ("SELECT dt, latitude, longitude, filename, frame "
                 "FROM frames "
                 "WHERE latitude > ? AND latitude < ? AND longitude > ? AND longitude < ? ")
        result = self._cnx.execute(query, (p3.y(), p1.y(), p4.x(), p2.x()))

        # Go through results and find the closest point
        rows = result.fetchall();
        closest_pt = {}
        closest_dist_m = max_dist_m
        distance = QgsDistanceArea()
        distance.setEllipsoid('WGS84')
        for row in rows:
            p = QgsPointXY(row[2], row[1])
            dist_m = distance.measureLine(center_deg, p)
            if dist_m < closest_dist_m:
                closest_pt['dt'] = row[0]
                closest_pt['latitude'] = row[1]
                closest_pt['longitude'] = row[2]
                closest_pt['filename'] = row[3]
                closest_pt['frame'] = row[4]
                closest_pt['dist_m'] = dist_m
                closest_dist_m = dist_m
        return closest_pt

class ShowCapturedImage(QgsMapToolEmitPoint):

    player: VideoPlayer = None

    def __init__(self, iface, dbs):

        self.iface = iface
        self.dbs = dbs
        QgsMapToolEmitPoint.__init__(self, self.iface.mapCanvas())

        # Conversion for changing epsg to WGS-84 latitude and longitude
        self.src_crs = QgsProject.instance().crs()
        self.dst_crs = QgsCoordinateReferenceSystem(4326)  # WGS 84
        self.x_form = QgsCoordinateTransform(self.src_crs, self.dst_crs, QgsProject.instance())

    def canvasPressEvent(self, e):
        point = self.toMapCoordinates(self.iface.mapCanvas().mouseLastXY())
        center_deg = self.x_form.transform(point)
        radius_m = 100 #meters
        closest_pts = {}
        for dataset, db in self.dbs.items():
            frame = db.get_closest_frame(center_deg, radius_m)
            if frame:
                print("Lat: %f  Lon: %f  File: %s" %
                      (frame['latitude'], frame['longitude'], frame['filename']))

                if not self.player:
                    self.player = VideoPlayer()
                self.player.setVideo(frame['filename'],frame['frame'])
                self.player.show()
            else:
                print(center_deg)
                print("No point found.")
          
class GroundskeeperBridge:
    """QGIS Plugin Implementation."""

    canvas_clicked = None

    dbs = {}

    bing_layer = None

    video_frames_db_name = "video_locations.sqlite"

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'GroundskeeperBridge_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)
            QCoreApplication.installTranslator(self.translator)

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&NWB Sensors Groundskeeper Bridge')

        # Check if plugin was started the first time in current QGIS session
        # Must be set in initGui() to survive plugin reloads
        self.first_start = None

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('GroundskeeperBridge', message)

    def add_action(
            self,
            icon_path,
            text,
            callback,
            enabled_flag=True,
            add_to_menu=True,
            add_to_toolbar=True,
            status_tip=None,
            whats_this=None,
            parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            # Adds plugin icon to Plugins toolbar
            self.iface.addToolBarIcon(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        # Add primary icon and callbacks
        self.add_action(
            ':plugins/nwb_gk_bridge/icon.png',
            text=self.tr(u'Import Groundskeeper data'),
            callback=self.run,
            parent=self.iface.mainWindow())

        # Add icon to activate image look up tool
        # Note: This is loaded directly instead of compiling. If desired, compile in the future.
        import pathlib
        folder = pathlib.Path(__file__).parent.resolve()
        self.add_action(
            str(folder / 'icon_select.png'),
            text=self.tr(u'Groundskeeper image lookup tool'),
            callback=self.run_image_select_tool,
            parent=self.iface.mainWindow())

        # will be set False in run()
        self.first_start = True

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(self.tr(u'&NWB Sensors Groundskeeper Bridge'), action)
            self.iface.removeToolBarIcon(action)

    @property
    def data_path(self):
        """
        Returns the location of the layer data files at created by this plugin for this project and ensure it exists
        """
        proj = QgsProject.instance()
        path = Path(proj.homePath()) / (proj.baseName() + "_data")
        path.mkdir(exist_ok=True)
        return path

    def check_project(self):
        projname = QgsProject.instance().baseName()
        if projname == '':
            show_messagebox('Project is not saved on disk. Please run the Groundskeeper bridge after you have created '
                            'a saved project.')
            return False;
        return True;

    def run_image_select_tool(self):
        """Activate image select tool
        """
        if(not self.check_project()):
            return;

        # Load the video location sqlite databases. They are keyed by project
        proj_datasets = list(self.data_path.iterdir())
        for dataset_path in proj_datasets:
            # print(dataset_path)
            db_file = dataset_path / self.video_frames_db_name;
            if(db_file.exists()):
                self.dbs[dataset_path] = VideoFramesSqliteDatabase(db_file);

        # Activate the select tool
        self.canvas_clicked = ShowCapturedImage(self.iface, self.dbs)
        self.iface.mapCanvas().setMapTool(self.canvas_clicked)

    def run(self):
        """Run method that performs all the real work
        """
        if (not self.check_project()):
            return;

        # Create the dialog with elements (after translation) and keep reference
        # Only create GUI ONCE in callback, so that it will only load when the plugin is started
        if self.first_start == True:
            self.first_start = False
            self.dlg = GroundskeeperBridgeDialog()
            self.dlg.inputVideoFolderSelector.setStorageMode(QgsFileWidget.GetDirectory)

        # Populate GUI with information
        projname = QgsProject.instance().baseName()
        self.dlg.projNameDisp.setText(projname)
        proj_datasets = list(self.data_path.iterdir())
        self.dlg.projDatasetCount.setText(str(len(proj_datasets)))

        # Clear menu and repopulate
        self.dlg.datasetList.clear()
        for dataset in proj_datasets:
            assert isinstance(dataset, Path)
            self.dlg.datasetList.addItem(str(dataset.name))

        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result:
            mode_idx = self.dlg.pluginMode.currentIndex()
            if mode_idx:
                self.delete_mode()
            else:
                self.import_mode()

    def import_mode(self):

        # Set up styles
        plugin_path = Path(__file__).parent.absolute()
        styles_path = plugin_path / 'styles'

        dataset = self.dlg.datasetName.text()
        if dataset == '':
            show_messagebox('Data set name must be populated')
            return
        elif len(re.findall('[&=#]', dataset)):
            show_messagebox('Data set name contains invalid characters.')
            return

        dataset_path = self.data_path / dataset
        if self.dlg.allowOverwrite.checkState():
            dataset_path.rmdir()
        if dataset_path.exists():
            show_messagebox('Layers already exist for this dataset. You must choose another dataset name or to overwrite the data.')
            return
        dataset_path.mkdir(exist_ok=True)

        # Create the layer CSVs from the Sqlite classsifier data
        input_path = Path(self.dlg.inputFileSelector.filePath())
        if not input_path.exists():
            raise FileNotFoundError('Could not find specified input file: ' + str(input_path))

        # Load the database
        db = ClassificationsSqliteConnection(input_path)

        thresh = self.dlg.confidenceThresh.value()
        thresh = float(thresh) / 100

        # Setup the progress dialog
        dialog = QProgressDialog()
        bar = QProgressBar(dialog)
        bar.setTextVisible(True)
        dialog.setBar(bar)
        dialog.setMinimumWidth(600)
        dialog.setMinimumDuration(500)
        dialog.show()

        # Always first add a Bing Maps tile layer if it doesn't exist
        dialog.setWindowTitle("Adding Bing maps aerial imagery")
        QApplication.processEvents()
        root = QgsProject.instance().layerTreeRoot()
        bing_name = 'Bing-Aerial'
        bing_exists = False
        for child in root.children():
            if child.name() == bing_name:
                bing_exists = True
                break;
        if not bing_exists:
            urlWithParams = 'type=xyz&url=http://ecn.t3.tiles.virtualearth.net/tiles/a%7Bq%7D.jpeg?g%3D1&zmax=19&zmin=0'
            self.bing_layer = QgsRasterLayer(urlWithParams, bing_name, 'wms')
            if self.bing_layer.isValid():
                QgsProject.instance().addMapLayer(self.bing_layer)
            else:
                print('Invalid layer. Can''t add Bing maps aerial imagery')
        bar.setValue(10)

        # Read database into CSV files
        dialog.setWindowTitle("Reading database")
        QApplication.processEvents()
        csv_data = {}
        labels = db.query("SELECT class, label FROM classifier_labels")
        input_data = db.query("SELECT best_class, best_confidence, latitude, longitude FROM classifications")
        for label in labels:
            csv_data[label[0]] = [x[1:] for x in input_data if x[0] == label[0] and x[1] >= thresh]

        for clss, data in csv_data.items():
            if len(data) != 0:
                with open(dataset_path.joinpath('%s.csv' % labels[clss][1]), "w+", newline="") as f:
                    w = csv.writer(f)
                    w.writerow(["Confidence", "Latitude", "Longitude"])
                    for row in data:
                        w.writerow(row)
        bar.setValue(25)

        # Create new group on the top
        dialog.setWindowTitle("Creating layers")
        QApplication.processEvents()
        series_group = root.insertGroup(0, dataset)
        groups = []
        for f in dataset_path.glob('*.csv'):
            layer_name = f.name.replace('.csv', '')
            categories = []
            current_group = series_group
            for i in range(0, layer_name.count("-")):
                if current_group.findGroup(
                        layer_name.split("-")[i]) is None:  # If there is not a group for a specified label category
                    current_group = current_group.addGroup(layer_name.split("-")[i])  # Make it and select it
                else:
                    current_group = current_group.findGroup(layer_name.split("-")[i])  # Otherwise just select it
        bar.setValue(50)

        # Render layers from the CSV files
        dialog.setWindowTitle("Rendering layers")
        QApplication.processEvents()
        layers = {}
        for f in dataset_path.glob('*.csv'):
            uri = 'file:///' + str(
                f) + '?type=csv&maxFields=10000&detectTypes=yes&xField=Longitude&yField=Latitude&crs=ESPG:4326&spatialIndex=no&subsetIndex=no&watchFile=no'
            full_layer_name = f.name.replace('.csv', '')
            layer_name = "%s-" % dataset + full_layer_name.split("-")[-1]
            if layer_name not in layers.keys():
                set_visible = False
                new_layer = QgsVectorLayer(uri, layer_name, 'delimitedtext')
                layers[layer_name] = new_layer
                # get_styles = QgsStyle.defaultStyle()
                # style = get_styles.symbol('topo pop capital')
                # new_layer.renderer().setSymbol(style)
                search_file = styles_path
                for p in full_layer_name.split("-"):
                    search_file = search_file / p
                search_file = Path(str(search_file) + ".qml")

                for d in range(0, len(full_layer_name.split("-"))):
                    if search_file.exists():
                        new_layer.loadNamedStyle(str(search_file))
                        set_visible = True
                        break
                    search_file = Path(str(search_file.parent) + ".qml")

                correct_cref = QgsCoordinateReferenceSystem(4326, QgsCoordinateReferenceSystem.PostgisCrsId)
                if not correct_cref.isValid():
                    raise RuntimeError('Invalid CRS detected. Submit the contents of this window to NWB Sensors.')
                new_layer.setCrs(correct_cref)

                QgsProject.instance().addMapLayer(new_layer, False)

                taxonomy = full_layer_name.split("-")
                curr_group = series_group.findGroup(full_layer_name.split("-")[-2]) if \
                    len(taxonomy) > 1 else series_group
                curr_group.addLayer(new_layer);
                root.findLayer(new_layer.id()).setItemVisibilityChecked(set_visible)

        # Load video databases and files
        if self.dlg.inputVideoFolderSelector.filePath():

            # Create the video locations sqlite database with the table for this dataset
            video_db_path = dataset_path / self.video_frames_db_name
            self.dbs[dataset_path] = VideoFramesSqliteDatabase(video_db_path)

            # Iterate through all sqlite files and matching video files. Create a database with meta data (lat, lon,
            # etc.) for all frames. Show progress dialog
            video_folder = Path(self.dlg.inputVideoFolderSelector.filePath())
            files = list(video_folder.glob('*.sqlite'))

            dialog.setWindowTitle("Merging video databases")
            for ff in range(len(files)):
                f = files[ff]
                dialog.setLabelText(str(f))
                self.dbs[dataset_path].merge_video_database(f)
                bar.setValue(50 + int(50 * ff / len(files)))
                QApplication.processEvents()

        # Refresh before finishing
        self.iface.mapCanvas().refresh()
        dialog.setWindowTitle("Done")
        bar.setValue(100)
        QApplication.processEvents()
        show_messagebox('Data load finished. Select a dataset from the ' + dataset + ' group to proceed.')

    def delete_mode(self):

        to_delete = self.dlg.datasetList.currentText()
        delete_path = self.data_path / to_delete
        assert delete_path.exists()

        proj = QgsProject.instance()
        root = proj.layerTreeRoot()
        for group in root.children():
            if group.name() == to_delete:
                for child in group.children():
                    proj.removeMapLayer(child.layerId())
                root.takeChild(group)
                self.dbs[delete_path].close()
                self.dbs.pop(delete_path, None);
                print(self.dbs)
                shutil.rmtree(delete_path)
                self.iface.mapCanvas().refresh()
                return;
        print("No group found. Group: " + to_delete)
